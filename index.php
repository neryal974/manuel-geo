<?
require_once 'geo-dataset.php';


//A l'ancienne
/* if(!empty($_GET['pays'])){
  $code_pays = $_GET['pays'];
  $pays = $liste_pays[$code_pays];
} else {
  $code_pays = '';
  $pays = '';
} */

//avec opérateur null coalescent
$code_pays = $_GET['pays'] ?? '';

//Condition ternaire
$pays = (isset($liste_pays[$code_pays])) ? $liste_pays[$code_pays] : '';

?>
<html>
<head>
  <title>Manuel de géographie</title>
  <link rel="stylesheet" href="style.css" />
</head>
<body>

<? require_once('header.php'); ?>
<div class="container">
<? require_once('liste-pays.php'); ?>
<? require_once('infos-pays.php'); ?>
</div>

</body>
</html>
